<?php

namespace App;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Input;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract
{
    use Authenticatable, CanResetPassword;
    public $prefix = "bn2_";
    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    public $incrementing = true;
    public $primaryKey = 'id';
    public $timestamps = true;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    public $table = 'users';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'name', 'email', 'password'];
    protected $hidden = ['password', 'remember_token'];

    function __construct($id = 0)
    {
        parent::__construct();

        $this->load($id);

        /**
         * Instantiate a new UserController instance.
         */
        //$this->beforeFilter('@checkRights');
    }

    function load($id = 0)
    {
        global $mysqli;

        $this->id = $id;
        if (($res = simpleQ($mysqli, "select * from " . Note::$prefix . "users where id=" . ((int)$id))) != NULL) {
            $row = mysqli_fetch_assoc($res);
        } else {
            $row = NULL;
        }
        if (($id > 0) && (($row !== null))) {
            $this->name = $row["name"];
            $this->email = $row["email"];

            $this->setAttribute('id', $row["id"]);
            $this->setAttribute("name", $row["name"]);
            $this->setAttribute("email", $row["email"]);


        } else {
            $this->id = 0;
            $this->name = $row["name"];
            $this->email = $row["email"];


            $this->setAttribute('id', 0);
            $this->setAttribute("name", $row["name"]);
            $this->setAttribute("email", $row["email"]);
        }
    }

    public static function getContacts($user_email)
    {
        global $mysqli;
        $realSQL = "select c.id as id, c.contact_name as name , c.email as email from " .
            Note::$prefix . "contacts as c inner join " . Note::$prefix . "users as u ON 
            u.id=c.owner_id where u.email='" . ($user_email) . "'";
        $res = simpleQ($mysqli, $realSQL);

        if ($res == NULL) {
            return NULL;
        }
        else {
            $arr = array();
            $i = 0;
            while (($row = mysqli_fetch_assoc($res)) != NULL) {
                $arr[$i] = $row;
                $i++;
            }

            return $arr;
        }
    }

    public static function findByEmail($email)
    {
        global $mysqli;
        $sql = "select * from " . Note::$prefix . "users" . " where email='" . $email . "'";

        $res = simpleQ($mysqli, $sql);
        if ($res != NULL) {
            $row = mysqli_fetch_assoc($res);
            $id = $row['id'];
            return new User($id);
        } else
            return new User(0);


    }
    public function sendEmailReminder($id)
    {
        $user = User::where('email', 'like', Input::get("email"))->get()->first();

        // Préparation du lien de rappel (reminder link)
        $reminder = new \App\Reminder($user->getAttribute('id'));
        $reminder->save(); // Save directly to track masssive attacks (on false email), has a cons: database recording.



        $user = User::findOrFail($id);

        $mail = $user->email; // Déclaration de l'adresse de destination.
        if (!preg_match("#^[a-z0-9._-]+@(hotmail|live|msn).[a-z]{2,4}$#", $mail)) // On filtre les serveurs qui rencontrent des bogues.
        {
            $passage_ligne = "\r\n";
        } else {
            $passage_ligne = "\n";
        }
//=====Déclaration des messages au format texte et au format HTML.
        $message_txt = "<p>Bonjour,</p>
 <p>voici votre lien pour renouveller votre mot de passe</p>
        <p>Si vous ne l'avez pas demandé ne vous inquiétez pas, ingorez-le.</p>
        <p>Sinon utilisez le lien suivant qui vous permettra
        de créer un nouveau mot de passe.</p>";
        $message_txt_link = $reminder->getLink() . '  >>>' . "  Lien de réinitialisation de mot de passe";
        $message_html_link = "<p><a href='" . ($reminder->getLink()) . "'>Lien de réinitialisation de mot de passe</a></p>";


        $message_html = "<html><head></head><body>" . $message_txt . $message_html_link . "</body></html>";
        $message_txt = str_replace('<p>', '', $message_txt);
        $message_txt = str_replace('</p>', '', $message_txt);
        $message_txt .= $message_txt_link;
//==========

//=====Création de la boundary
        $boundary = "-----=" . md5(rand());
//==========

//=====Définition du sujet.
        $sujet = "Hey mon ami !";
//=========

//=====Création du header de l'e-mail.
        $header = "From: \"Ibiteria\"<noreply@ibiteria.com>" . $passage_ligne;
        $header .= "Reply-to: \"Ibiteria\" <ibiteria.com>" . $passage_ligne;
        $header .= "MIME-Version: 1.0" . $passage_ligne;
        $header .= "Content-Type: multipart/alternative;" . $passage_ligne . " boundary=\"$boundary\"" . $passage_ligne;
//==========

//=====Création du message.
        $message = $passage_ligne . "--" . $boundary . $passage_ligne;
//=====Ajout du message au format texte.
        $message .= "Content-Type: text/plain; charset=\"ISO-8859-1\"" . $passage_ligne;
        $message .= "Content-Transfer-Encoding: 8bit" . $passage_ligne;
        $message .= $passage_ligne . $message_txt . $passage_ligne;
//==========
        $message .= $passage_ligne . "--" . $boundary . $passage_ligne;
//=====Ajout du message au format HTML
        $message .= "Content-Type: text/html; charset=\"ISO-8859-1\"" . $passage_ligne;
        $message .= "Content-Transfer-Encoding: 8bit" . $passage_ligne;
        $message .= $passage_ligne . $message_html . $passage_ligne;
//==========
        $message .= $passage_ligne . "--" . $boundary . "--" . $passage_ligne;
        $message .= $passage_ligne . "--" . $boundary . "--" . $passage_ligne;
//==========
        echo '<textarea>' . $message . '</textarea>';
//=====Envoi de l'e-mail.
        if (mail($mail, $sujet, $message, $header)) {
            $message_err = "OK";
            $ret = true;

        } else {
            $message_err = "Erreur lors de l'envoi du mail";
            $ret = false;
        }
//==========
        return $ret;
    }

    public function sendEmailInvitation(Share $share)
    {
        $user = User::where('email', 'like', Input::get("email"))->get()->first();

        // Préparation du lien de rappel (reminder link)
        $reminder = new \App\Reminder($user->getAttribute('id'));
        $reminder->save(); // Save directly to track masssive attacks (on false email), has a cons: database recording.


        $partageur = User::find($share->getAttribute(User::findOrNew(note_id)->getAttribute("email")));

        if ($user == null || $user->getAttribute("id") <= 0) {
            die("erreur");

        }

        $mail = $user->email; // Déclaration de l'adresse de destination.
        if (!preg_match("#^[a-z0-9._-]+@(hotmail|live|msn).[a-z]{2,4}$#", $mail)) // On filtre les serveurs qui rencontrent des bogues.
        {
            $passage_ligne = "\r\n";
        } else {
            $passage_ligne = "\n";
        }
//=====Déclaration des messages au format texte et au format HTML.
        $message_txt = "<p>Bonjour,</p>
 <p>Vous êtes invité à rejoindre l'application BlocFish - BlokFiches - etc sur le site www.ibiteria.com</p>
        <p>Si ça ne vous intéresse pas, ingorez-le.</p>
        <p>Sinon la note " . ($share->getAttribute("note_id")) . "
a été partagée avec vous.</p>";
        $message_txt_link = $reminder->getLink() . '  >>>' . "  Lien partagé";
        $message_html_link = "<p><a href='" . ($reminder->getLink()) . "'>Lien de réinitialisation de mot de passe</a></p>";


        $message_html = "<html><head></head><body>" . $message_txt . $message_html_link . "</body></html>";
        $message_txt = str_replace('<p>', '', $message_txt);
        $message_txt = str_replace('</p>', '', $message_txt);
        $message_txt .= $message_txt_link;
//==========

//=====Création de la boundary
        $boundary = "-----=" . md5(rand());
//==========

//=====Définition du sujet.
        $sujet = "ibiteria.com - Message informatif (partage de note) $partageur";
//=========

//=====Création du header de l'e-mail.
        $header = "From: \"Ibiteria\"<noreply@ibiteria.com>" . $passage_ligne;
        $header .= "Reply-to: \"Ibiteria\" <ibiteria.com>" . $passage_ligne;
        $header .= "MIME-Version: 1.0" . $passage_ligne;
        $header .= "Content-Type: multipart/alternative;" . $passage_ligne . " boundary=\"$boundary\"" . $passage_ligne;
//==========

//=====Création du message.
        $message = $passage_ligne . "--" . $boundary . $passage_ligne;
//=====Ajout du message au format texte.
        $message .= "Content-Type: text/plain; charset=\"ISO-8859-1\"" . $passage_ligne;
        $message .= "Content-Transfer-Encoding: 8bit" . $passage_ligne;
        $message .= $passage_ligne . $message_txt . $passage_ligne;
//==========
        $message .= $passage_ligne . "--" . $boundary . $passage_ligne;
//=====Ajout du message au format HTML
        $message .= "Content-Type: text/html; charset=\"ISO-8859-1\"" . $passage_ligne;
        $message .= "Content-Transfer-Encoding: 8bit" . $passage_ligne;
        $message .= $passage_ligne . $message_html . $passage_ligne;
//==========
        $message .= $passage_ligne . "--" . $boundary . "--" . $passage_ligne;
        $message .= $passage_ligne . "--" . $boundary . "--" . $passage_ligne;
//==========
        echo '<textarea>' . $message . '</textarea>';
//=====Envoi de l'e-mail.
        if (mail($mail, $sujet, $message, $header)) {
            $message_err = "OK";
            $ret = true;

        } else {
            $message_err = "Erreur lors de l'envoi du mail";
            $ret = false;
        }
//==========
        return $ret;
    }

    public function getRights($note_id)
    {
    }

}
